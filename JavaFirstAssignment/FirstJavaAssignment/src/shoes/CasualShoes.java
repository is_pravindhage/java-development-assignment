package shoes;

public class CasualShoes extends Shoes{

	String loafer, moccasin;

	public String getLoafer() {
		return loafer;
	}

	public void setLoafer(String loafer) {
		this.loafer = loafer;
	}

	public String getMoccasin() {
		return moccasin;
	}

	public void setMoccasin(String moccasin) {
		this.moccasin = moccasin;
	}

	public void display() {
		System.out.println("CasualShoes information display successfully");
	}
	
}
