package shoes;

public class Shoes {

	public static void main(String args[]) {

		SportsShoes sportsShoes = new SportsShoes();
		sportsShoes.display();
		SportsShoes sportsShoes1 = new SportsShoes("HRX");
		sportsShoes1.display();
		FormalShoes formalShoes = new FormalShoes();
		formalShoes.display();
		CasualShoes casualShoes = new CasualShoes();
		casualShoes.display();
	}

}
